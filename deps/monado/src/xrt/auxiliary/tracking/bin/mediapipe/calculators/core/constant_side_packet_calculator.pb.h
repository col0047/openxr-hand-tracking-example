// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: mediapipe/calculators/core/constant_side_packet_calculator.proto

#ifndef GOOGLE_PROTOBUF_INCLUDED_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto
#define GOOGLE_PROTOBUF_INCLUDED_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto

#include <limits>
#include <string>

#include <google/protobuf/port_def.inc>
#if PROTOBUF_VERSION < 3011000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers. Please update
#error your headers.
#endif
#if 3011004 < PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers. Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/port_undef.inc>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_table_driven.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/inlined_string_field.h>
#include <google/protobuf/metadata.h>
#include <google/protobuf/generated_message_reflection.h>
#include <google/protobuf/message.h>
#include <google/protobuf/repeated_field.h>  // IWYU pragma: export
#include <google/protobuf/extension_set.h>  // IWYU pragma: export
#include <google/protobuf/unknown_field_set.h>
#include "mediapipe/framework/calculator.pb.h"
#include "mediapipe/framework/formats/classification.pb.h"
// @@protoc_insertion_point(includes)
#include <google/protobuf/port_def.inc>
#define PROTOBUF_INTERNAL_EXPORT_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto
PROTOBUF_NAMESPACE_OPEN
namespace internal {
class AnyMetadata;
}  // namespace internal
PROTOBUF_NAMESPACE_CLOSE

// Internal implementation detail -- do not use these members.
struct TableStruct_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto {
  static const ::PROTOBUF_NAMESPACE_ID::internal::ParseTableField entries[]
    PROTOBUF_SECTION_VARIABLE(protodesc_cold);
  static const ::PROTOBUF_NAMESPACE_ID::internal::AuxillaryParseTableField aux[]
    PROTOBUF_SECTION_VARIABLE(protodesc_cold);
  static const ::PROTOBUF_NAMESPACE_ID::internal::ParseTable schema[2]
    PROTOBUF_SECTION_VARIABLE(protodesc_cold);
  static const ::PROTOBUF_NAMESPACE_ID::internal::FieldMetadata field_metadata[];
  static const ::PROTOBUF_NAMESPACE_ID::internal::SerializationTable serialization_table[];
  static const ::PROTOBUF_NAMESPACE_ID::uint32 offsets[];
};
extern const ::PROTOBUF_NAMESPACE_ID::internal::DescriptorTable descriptor_table_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto;
namespace mediapipe {
class ConstantSidePacketCalculatorOptions;
class ConstantSidePacketCalculatorOptionsDefaultTypeInternal;
extern ConstantSidePacketCalculatorOptionsDefaultTypeInternal _ConstantSidePacketCalculatorOptions_default_instance_;
class ConstantSidePacketCalculatorOptions_ConstantSidePacket;
class ConstantSidePacketCalculatorOptions_ConstantSidePacketDefaultTypeInternal;
extern ConstantSidePacketCalculatorOptions_ConstantSidePacketDefaultTypeInternal _ConstantSidePacketCalculatorOptions_ConstantSidePacket_default_instance_;
}  // namespace mediapipe
PROTOBUF_NAMESPACE_OPEN
template<> ::mediapipe::ConstantSidePacketCalculatorOptions* Arena::CreateMaybeMessage<::mediapipe::ConstantSidePacketCalculatorOptions>(Arena*);
template<> ::mediapipe::ConstantSidePacketCalculatorOptions_ConstantSidePacket* Arena::CreateMaybeMessage<::mediapipe::ConstantSidePacketCalculatorOptions_ConstantSidePacket>(Arena*);
PROTOBUF_NAMESPACE_CLOSE
namespace mediapipe {

// ===================================================================

class ConstantSidePacketCalculatorOptions_ConstantSidePacket :
    public ::PROTOBUF_NAMESPACE_ID::Message /* @@protoc_insertion_point(class_definition:mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket) */ {
 public:
  ConstantSidePacketCalculatorOptions_ConstantSidePacket();
  virtual ~ConstantSidePacketCalculatorOptions_ConstantSidePacket();

  ConstantSidePacketCalculatorOptions_ConstantSidePacket(const ConstantSidePacketCalculatorOptions_ConstantSidePacket& from);
  ConstantSidePacketCalculatorOptions_ConstantSidePacket(ConstantSidePacketCalculatorOptions_ConstantSidePacket&& from) noexcept
    : ConstantSidePacketCalculatorOptions_ConstantSidePacket() {
    *this = ::std::move(from);
  }

  inline ConstantSidePacketCalculatorOptions_ConstantSidePacket& operator=(const ConstantSidePacketCalculatorOptions_ConstantSidePacket& from) {
    CopyFrom(from);
    return *this;
  }
  inline ConstantSidePacketCalculatorOptions_ConstantSidePacket& operator=(ConstantSidePacketCalculatorOptions_ConstantSidePacket&& from) noexcept {
    if (GetArenaNoVirtual() == from.GetArenaNoVirtual()) {
      if (this != &from) InternalSwap(&from);
    } else {
      CopyFrom(from);
    }
    return *this;
  }

  inline const ::PROTOBUF_NAMESPACE_ID::UnknownFieldSet& unknown_fields() const {
    return _internal_metadata_.unknown_fields();
  }
  inline ::PROTOBUF_NAMESPACE_ID::UnknownFieldSet* mutable_unknown_fields() {
    return _internal_metadata_.mutable_unknown_fields();
  }

  static const ::PROTOBUF_NAMESPACE_ID::Descriptor* descriptor() {
    return GetDescriptor();
  }
  static const ::PROTOBUF_NAMESPACE_ID::Descriptor* GetDescriptor() {
    return GetMetadataStatic().descriptor;
  }
  static const ::PROTOBUF_NAMESPACE_ID::Reflection* GetReflection() {
    return GetMetadataStatic().reflection;
  }
  static const ConstantSidePacketCalculatorOptions_ConstantSidePacket& default_instance();

  enum ValueCase {
    kIntValue = 1,
    kFloatValue = 2,
    kBoolValue = 3,
    kStringValue = 4,
    kUint64Value = 5,
    kClassificationListValue = 6,
    VALUE_NOT_SET = 0,
  };

  static void InitAsDefaultInstance();  // FOR INTERNAL USE ONLY
  static inline const ConstantSidePacketCalculatorOptions_ConstantSidePacket* internal_default_instance() {
    return reinterpret_cast<const ConstantSidePacketCalculatorOptions_ConstantSidePacket*>(
               &_ConstantSidePacketCalculatorOptions_ConstantSidePacket_default_instance_);
  }
  static constexpr int kIndexInFileMessages =
    0;

  friend void swap(ConstantSidePacketCalculatorOptions_ConstantSidePacket& a, ConstantSidePacketCalculatorOptions_ConstantSidePacket& b) {
    a.Swap(&b);
  }
  inline void Swap(ConstantSidePacketCalculatorOptions_ConstantSidePacket* other) {
    if (other == this) return;
    InternalSwap(other);
  }

  // implements Message ----------------------------------------------

  inline ConstantSidePacketCalculatorOptions_ConstantSidePacket* New() const final {
    return CreateMaybeMessage<ConstantSidePacketCalculatorOptions_ConstantSidePacket>(nullptr);
  }

  ConstantSidePacketCalculatorOptions_ConstantSidePacket* New(::PROTOBUF_NAMESPACE_ID::Arena* arena) const final {
    return CreateMaybeMessage<ConstantSidePacketCalculatorOptions_ConstantSidePacket>(arena);
  }
  void CopyFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) final;
  void MergeFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) final;
  void CopyFrom(const ConstantSidePacketCalculatorOptions_ConstantSidePacket& from);
  void MergeFrom(const ConstantSidePacketCalculatorOptions_ConstantSidePacket& from);
  PROTOBUF_ATTRIBUTE_REINITIALIZES void Clear() final;
  bool IsInitialized() const final;

  size_t ByteSizeLong() const final;
  const char* _InternalParse(const char* ptr, ::PROTOBUF_NAMESPACE_ID::internal::ParseContext* ctx) final;
  ::PROTOBUF_NAMESPACE_ID::uint8* _InternalSerialize(
      ::PROTOBUF_NAMESPACE_ID::uint8* target, ::PROTOBUF_NAMESPACE_ID::io::EpsCopyOutputStream* stream) const final;
  int GetCachedSize() const final { return _cached_size_.Get(); }

  private:
  inline void SharedCtor();
  inline void SharedDtor();
  void SetCachedSize(int size) const final;
  void InternalSwap(ConstantSidePacketCalculatorOptions_ConstantSidePacket* other);
  friend class ::PROTOBUF_NAMESPACE_ID::internal::AnyMetadata;
  static ::PROTOBUF_NAMESPACE_ID::StringPiece FullMessageName() {
    return "mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket";
  }
  private:
  inline ::PROTOBUF_NAMESPACE_ID::Arena* GetArenaNoVirtual() const {
    return nullptr;
  }
  inline void* MaybeArenaPtr() const {
    return nullptr;
  }
  public:

  ::PROTOBUF_NAMESPACE_ID::Metadata GetMetadata() const final;
  private:
  static ::PROTOBUF_NAMESPACE_ID::Metadata GetMetadataStatic() {
    ::PROTOBUF_NAMESPACE_ID::internal::AssignDescriptors(&::descriptor_table_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto);
    return ::descriptor_table_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto.file_level_metadata[kIndexInFileMessages];
  }

  public:

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  enum : int {
    kIntValueFieldNumber = 1,
    kFloatValueFieldNumber = 2,
    kBoolValueFieldNumber = 3,
    kStringValueFieldNumber = 4,
    kUint64ValueFieldNumber = 5,
    kClassificationListValueFieldNumber = 6,
  };
  // optional int32 int_value = 1;
  bool has_int_value() const;
  private:
  bool _internal_has_int_value() const;
  public:
  void clear_int_value();
  ::PROTOBUF_NAMESPACE_ID::int32 int_value() const;
  void set_int_value(::PROTOBUF_NAMESPACE_ID::int32 value);
  private:
  ::PROTOBUF_NAMESPACE_ID::int32 _internal_int_value() const;
  void _internal_set_int_value(::PROTOBUF_NAMESPACE_ID::int32 value);
  public:

  // optional float float_value = 2;
  bool has_float_value() const;
  private:
  bool _internal_has_float_value() const;
  public:
  void clear_float_value();
  float float_value() const;
  void set_float_value(float value);
  private:
  float _internal_float_value() const;
  void _internal_set_float_value(float value);
  public:

  // optional bool bool_value = 3;
  bool has_bool_value() const;
  private:
  bool _internal_has_bool_value() const;
  public:
  void clear_bool_value();
  bool bool_value() const;
  void set_bool_value(bool value);
  private:
  bool _internal_bool_value() const;
  void _internal_set_bool_value(bool value);
  public:

  // optional string string_value = 4;
  bool has_string_value() const;
  private:
  bool _internal_has_string_value() const;
  public:
  void clear_string_value();
  const std::string& string_value() const;
  void set_string_value(const std::string& value);
  void set_string_value(std::string&& value);
  void set_string_value(const char* value);
  void set_string_value(const char* value, size_t size);
  std::string* mutable_string_value();
  std::string* release_string_value();
  void set_allocated_string_value(std::string* string_value);
  private:
  const std::string& _internal_string_value() const;
  void _internal_set_string_value(const std::string& value);
  std::string* _internal_mutable_string_value();
  public:

  // optional uint64 uint64_value = 5;
  bool has_uint64_value() const;
  private:
  bool _internal_has_uint64_value() const;
  public:
  void clear_uint64_value();
  ::PROTOBUF_NAMESPACE_ID::uint64 uint64_value() const;
  void set_uint64_value(::PROTOBUF_NAMESPACE_ID::uint64 value);
  private:
  ::PROTOBUF_NAMESPACE_ID::uint64 _internal_uint64_value() const;
  void _internal_set_uint64_value(::PROTOBUF_NAMESPACE_ID::uint64 value);
  public:

  // optional .mediapipe.ClassificationList classification_list_value = 6;
  bool has_classification_list_value() const;
  private:
  bool _internal_has_classification_list_value() const;
  public:
  void clear_classification_list_value();
  const ::mediapipe::ClassificationList& classification_list_value() const;
  ::mediapipe::ClassificationList* release_classification_list_value();
  ::mediapipe::ClassificationList* mutable_classification_list_value();
  void set_allocated_classification_list_value(::mediapipe::ClassificationList* classification_list_value);
  private:
  const ::mediapipe::ClassificationList& _internal_classification_list_value() const;
  ::mediapipe::ClassificationList* _internal_mutable_classification_list_value();
  public:

  void clear_value();
  ValueCase value_case() const;
  // @@protoc_insertion_point(class_scope:mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket)
 private:
  class _Internal;
  void set_has_int_value();
  void set_has_float_value();
  void set_has_bool_value();
  void set_has_string_value();
  void set_has_uint64_value();
  void set_has_classification_list_value();

  inline bool has_value() const;
  inline void clear_has_value();

  ::PROTOBUF_NAMESPACE_ID::internal::InternalMetadataWithArena _internal_metadata_;
  ::PROTOBUF_NAMESPACE_ID::internal::HasBits<1> _has_bits_;
  mutable ::PROTOBUF_NAMESPACE_ID::internal::CachedSize _cached_size_;
  union ValueUnion {
    ValueUnion() {}
    ::PROTOBUF_NAMESPACE_ID::int32 int_value_;
    float float_value_;
    bool bool_value_;
    ::PROTOBUF_NAMESPACE_ID::internal::ArenaStringPtr string_value_;
    ::PROTOBUF_NAMESPACE_ID::uint64 uint64_value_;
    ::mediapipe::ClassificationList* classification_list_value_;
  } value_;
  ::PROTOBUF_NAMESPACE_ID::uint32 _oneof_case_[1];

  friend struct ::TableStruct_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto;
};
// -------------------------------------------------------------------

class ConstantSidePacketCalculatorOptions :
    public ::PROTOBUF_NAMESPACE_ID::Message /* @@protoc_insertion_point(class_definition:mediapipe.ConstantSidePacketCalculatorOptions) */ {
 public:
  ConstantSidePacketCalculatorOptions();
  virtual ~ConstantSidePacketCalculatorOptions();

  ConstantSidePacketCalculatorOptions(const ConstantSidePacketCalculatorOptions& from);
  ConstantSidePacketCalculatorOptions(ConstantSidePacketCalculatorOptions&& from) noexcept
    : ConstantSidePacketCalculatorOptions() {
    *this = ::std::move(from);
  }

  inline ConstantSidePacketCalculatorOptions& operator=(const ConstantSidePacketCalculatorOptions& from) {
    CopyFrom(from);
    return *this;
  }
  inline ConstantSidePacketCalculatorOptions& operator=(ConstantSidePacketCalculatorOptions&& from) noexcept {
    if (GetArenaNoVirtual() == from.GetArenaNoVirtual()) {
      if (this != &from) InternalSwap(&from);
    } else {
      CopyFrom(from);
    }
    return *this;
  }

  inline const ::PROTOBUF_NAMESPACE_ID::UnknownFieldSet& unknown_fields() const {
    return _internal_metadata_.unknown_fields();
  }
  inline ::PROTOBUF_NAMESPACE_ID::UnknownFieldSet* mutable_unknown_fields() {
    return _internal_metadata_.mutable_unknown_fields();
  }

  static const ::PROTOBUF_NAMESPACE_ID::Descriptor* descriptor() {
    return GetDescriptor();
  }
  static const ::PROTOBUF_NAMESPACE_ID::Descriptor* GetDescriptor() {
    return GetMetadataStatic().descriptor;
  }
  static const ::PROTOBUF_NAMESPACE_ID::Reflection* GetReflection() {
    return GetMetadataStatic().reflection;
  }
  static const ConstantSidePacketCalculatorOptions& default_instance();

  static void InitAsDefaultInstance();  // FOR INTERNAL USE ONLY
  static inline const ConstantSidePacketCalculatorOptions* internal_default_instance() {
    return reinterpret_cast<const ConstantSidePacketCalculatorOptions*>(
               &_ConstantSidePacketCalculatorOptions_default_instance_);
  }
  static constexpr int kIndexInFileMessages =
    1;

  friend void swap(ConstantSidePacketCalculatorOptions& a, ConstantSidePacketCalculatorOptions& b) {
    a.Swap(&b);
  }
  inline void Swap(ConstantSidePacketCalculatorOptions* other) {
    if (other == this) return;
    InternalSwap(other);
  }

  // implements Message ----------------------------------------------

  inline ConstantSidePacketCalculatorOptions* New() const final {
    return CreateMaybeMessage<ConstantSidePacketCalculatorOptions>(nullptr);
  }

  ConstantSidePacketCalculatorOptions* New(::PROTOBUF_NAMESPACE_ID::Arena* arena) const final {
    return CreateMaybeMessage<ConstantSidePacketCalculatorOptions>(arena);
  }
  void CopyFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) final;
  void MergeFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) final;
  void CopyFrom(const ConstantSidePacketCalculatorOptions& from);
  void MergeFrom(const ConstantSidePacketCalculatorOptions& from);
  PROTOBUF_ATTRIBUTE_REINITIALIZES void Clear() final;
  bool IsInitialized() const final;

  size_t ByteSizeLong() const final;
  const char* _InternalParse(const char* ptr, ::PROTOBUF_NAMESPACE_ID::internal::ParseContext* ctx) final;
  ::PROTOBUF_NAMESPACE_ID::uint8* _InternalSerialize(
      ::PROTOBUF_NAMESPACE_ID::uint8* target, ::PROTOBUF_NAMESPACE_ID::io::EpsCopyOutputStream* stream) const final;
  int GetCachedSize() const final { return _cached_size_.Get(); }

  private:
  inline void SharedCtor();
  inline void SharedDtor();
  void SetCachedSize(int size) const final;
  void InternalSwap(ConstantSidePacketCalculatorOptions* other);
  friend class ::PROTOBUF_NAMESPACE_ID::internal::AnyMetadata;
  static ::PROTOBUF_NAMESPACE_ID::StringPiece FullMessageName() {
    return "mediapipe.ConstantSidePacketCalculatorOptions";
  }
  private:
  inline ::PROTOBUF_NAMESPACE_ID::Arena* GetArenaNoVirtual() const {
    return nullptr;
  }
  inline void* MaybeArenaPtr() const {
    return nullptr;
  }
  public:

  ::PROTOBUF_NAMESPACE_ID::Metadata GetMetadata() const final;
  private:
  static ::PROTOBUF_NAMESPACE_ID::Metadata GetMetadataStatic() {
    ::PROTOBUF_NAMESPACE_ID::internal::AssignDescriptors(&::descriptor_table_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto);
    return ::descriptor_table_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto.file_level_metadata[kIndexInFileMessages];
  }

  public:

  // nested types ----------------------------------------------------

  typedef ConstantSidePacketCalculatorOptions_ConstantSidePacket ConstantSidePacket;

  // accessors -------------------------------------------------------

  enum : int {
    kPacketFieldNumber = 1,
  };
  // repeated .mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket packet = 1;
  int packet_size() const;
  private:
  int _internal_packet_size() const;
  public:
  void clear_packet();
  ::mediapipe::ConstantSidePacketCalculatorOptions_ConstantSidePacket* mutable_packet(int index);
  ::PROTOBUF_NAMESPACE_ID::RepeatedPtrField< ::mediapipe::ConstantSidePacketCalculatorOptions_ConstantSidePacket >*
      mutable_packet();
  private:
  const ::mediapipe::ConstantSidePacketCalculatorOptions_ConstantSidePacket& _internal_packet(int index) const;
  ::mediapipe::ConstantSidePacketCalculatorOptions_ConstantSidePacket* _internal_add_packet();
  public:
  const ::mediapipe::ConstantSidePacketCalculatorOptions_ConstantSidePacket& packet(int index) const;
  ::mediapipe::ConstantSidePacketCalculatorOptions_ConstantSidePacket* add_packet();
  const ::PROTOBUF_NAMESPACE_ID::RepeatedPtrField< ::mediapipe::ConstantSidePacketCalculatorOptions_ConstantSidePacket >&
      packet() const;

  static const int kExtFieldNumber = 291214597;
  static ::PROTOBUF_NAMESPACE_ID::internal::ExtensionIdentifier< ::mediapipe::CalculatorOptions,
      ::PROTOBUF_NAMESPACE_ID::internal::MessageTypeTraits< ::mediapipe::ConstantSidePacketCalculatorOptions >, 11, false >
    ext;
  // @@protoc_insertion_point(class_scope:mediapipe.ConstantSidePacketCalculatorOptions)
 private:
  class _Internal;

  ::PROTOBUF_NAMESPACE_ID::internal::InternalMetadataWithArena _internal_metadata_;
  ::PROTOBUF_NAMESPACE_ID::internal::HasBits<1> _has_bits_;
  mutable ::PROTOBUF_NAMESPACE_ID::internal::CachedSize _cached_size_;
  ::PROTOBUF_NAMESPACE_ID::RepeatedPtrField< ::mediapipe::ConstantSidePacketCalculatorOptions_ConstantSidePacket > packet_;
  friend struct ::TableStruct_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto;
};
// ===================================================================


// ===================================================================

#ifdef __GNUC__
  #pragma GCC diagnostic push
  #pragma GCC diagnostic ignored "-Wstrict-aliasing"
#endif  // __GNUC__
// ConstantSidePacketCalculatorOptions_ConstantSidePacket

// optional int32 int_value = 1;
inline bool ConstantSidePacketCalculatorOptions_ConstantSidePacket::_internal_has_int_value() const {
  return value_case() == kIntValue;
}
inline bool ConstantSidePacketCalculatorOptions_ConstantSidePacket::has_int_value() const {
  return _internal_has_int_value();
}
inline void ConstantSidePacketCalculatorOptions_ConstantSidePacket::set_has_int_value() {
  _oneof_case_[0] = kIntValue;
}
inline void ConstantSidePacketCalculatorOptions_ConstantSidePacket::clear_int_value() {
  if (_internal_has_int_value()) {
    value_.int_value_ = 0;
    clear_has_value();
  }
}
inline ::PROTOBUF_NAMESPACE_ID::int32 ConstantSidePacketCalculatorOptions_ConstantSidePacket::_internal_int_value() const {
  if (_internal_has_int_value()) {
    return value_.int_value_;
  }
  return 0;
}
inline void ConstantSidePacketCalculatorOptions_ConstantSidePacket::_internal_set_int_value(::PROTOBUF_NAMESPACE_ID::int32 value) {
  if (!_internal_has_int_value()) {
    clear_value();
    set_has_int_value();
  }
  value_.int_value_ = value;
}
inline ::PROTOBUF_NAMESPACE_ID::int32 ConstantSidePacketCalculatorOptions_ConstantSidePacket::int_value() const {
  // @@protoc_insertion_point(field_get:mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket.int_value)
  return _internal_int_value();
}
inline void ConstantSidePacketCalculatorOptions_ConstantSidePacket::set_int_value(::PROTOBUF_NAMESPACE_ID::int32 value) {
  _internal_set_int_value(value);
  // @@protoc_insertion_point(field_set:mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket.int_value)
}

// optional float float_value = 2;
inline bool ConstantSidePacketCalculatorOptions_ConstantSidePacket::_internal_has_float_value() const {
  return value_case() == kFloatValue;
}
inline bool ConstantSidePacketCalculatorOptions_ConstantSidePacket::has_float_value() const {
  return _internal_has_float_value();
}
inline void ConstantSidePacketCalculatorOptions_ConstantSidePacket::set_has_float_value() {
  _oneof_case_[0] = kFloatValue;
}
inline void ConstantSidePacketCalculatorOptions_ConstantSidePacket::clear_float_value() {
  if (_internal_has_float_value()) {
    value_.float_value_ = 0;
    clear_has_value();
  }
}
inline float ConstantSidePacketCalculatorOptions_ConstantSidePacket::_internal_float_value() const {
  if (_internal_has_float_value()) {
    return value_.float_value_;
  }
  return 0;
}
inline void ConstantSidePacketCalculatorOptions_ConstantSidePacket::_internal_set_float_value(float value) {
  if (!_internal_has_float_value()) {
    clear_value();
    set_has_float_value();
  }
  value_.float_value_ = value;
}
inline float ConstantSidePacketCalculatorOptions_ConstantSidePacket::float_value() const {
  // @@protoc_insertion_point(field_get:mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket.float_value)
  return _internal_float_value();
}
inline void ConstantSidePacketCalculatorOptions_ConstantSidePacket::set_float_value(float value) {
  _internal_set_float_value(value);
  // @@protoc_insertion_point(field_set:mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket.float_value)
}

// optional bool bool_value = 3;
inline bool ConstantSidePacketCalculatorOptions_ConstantSidePacket::_internal_has_bool_value() const {
  return value_case() == kBoolValue;
}
inline bool ConstantSidePacketCalculatorOptions_ConstantSidePacket::has_bool_value() const {
  return _internal_has_bool_value();
}
inline void ConstantSidePacketCalculatorOptions_ConstantSidePacket::set_has_bool_value() {
  _oneof_case_[0] = kBoolValue;
}
inline void ConstantSidePacketCalculatorOptions_ConstantSidePacket::clear_bool_value() {
  if (_internal_has_bool_value()) {
    value_.bool_value_ = false;
    clear_has_value();
  }
}
inline bool ConstantSidePacketCalculatorOptions_ConstantSidePacket::_internal_bool_value() const {
  if (_internal_has_bool_value()) {
    return value_.bool_value_;
  }
  return false;
}
inline void ConstantSidePacketCalculatorOptions_ConstantSidePacket::_internal_set_bool_value(bool value) {
  if (!_internal_has_bool_value()) {
    clear_value();
    set_has_bool_value();
  }
  value_.bool_value_ = value;
}
inline bool ConstantSidePacketCalculatorOptions_ConstantSidePacket::bool_value() const {
  // @@protoc_insertion_point(field_get:mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket.bool_value)
  return _internal_bool_value();
}
inline void ConstantSidePacketCalculatorOptions_ConstantSidePacket::set_bool_value(bool value) {
  _internal_set_bool_value(value);
  // @@protoc_insertion_point(field_set:mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket.bool_value)
}

// optional string string_value = 4;
inline bool ConstantSidePacketCalculatorOptions_ConstantSidePacket::_internal_has_string_value() const {
  return value_case() == kStringValue;
}
inline bool ConstantSidePacketCalculatorOptions_ConstantSidePacket::has_string_value() const {
  return _internal_has_string_value();
}
inline void ConstantSidePacketCalculatorOptions_ConstantSidePacket::set_has_string_value() {
  _oneof_case_[0] = kStringValue;
}
inline void ConstantSidePacketCalculatorOptions_ConstantSidePacket::clear_string_value() {
  if (_internal_has_string_value()) {
    value_.string_value_.DestroyNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
    clear_has_value();
  }
}
inline const std::string& ConstantSidePacketCalculatorOptions_ConstantSidePacket::string_value() const {
  // @@protoc_insertion_point(field_get:mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket.string_value)
  return _internal_string_value();
}
inline void ConstantSidePacketCalculatorOptions_ConstantSidePacket::set_string_value(const std::string& value) {
  _internal_set_string_value(value);
  // @@protoc_insertion_point(field_set:mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket.string_value)
}
inline std::string* ConstantSidePacketCalculatorOptions_ConstantSidePacket::mutable_string_value() {
  // @@protoc_insertion_point(field_mutable:mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket.string_value)
  return _internal_mutable_string_value();
}
inline const std::string& ConstantSidePacketCalculatorOptions_ConstantSidePacket::_internal_string_value() const {
  if (_internal_has_string_value()) {
    return value_.string_value_.GetNoArena();
  }
  return *&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited();
}
inline void ConstantSidePacketCalculatorOptions_ConstantSidePacket::_internal_set_string_value(const std::string& value) {
  if (!_internal_has_string_value()) {
    clear_value();
    set_has_string_value();
    value_.string_value_.UnsafeSetDefault(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
  }
  value_.string_value_.SetNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), value);
}
inline void ConstantSidePacketCalculatorOptions_ConstantSidePacket::set_string_value(std::string&& value) {
  // @@protoc_insertion_point(field_set:mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket.string_value)
  if (!_internal_has_string_value()) {
    clear_value();
    set_has_string_value();
    value_.string_value_.UnsafeSetDefault(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
  }
  value_.string_value_.SetNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), ::std::move(value));
  // @@protoc_insertion_point(field_set_rvalue:mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket.string_value)
}
inline void ConstantSidePacketCalculatorOptions_ConstantSidePacket::set_string_value(const char* value) {
  GOOGLE_DCHECK(value != nullptr);
  if (!_internal_has_string_value()) {
    clear_value();
    set_has_string_value();
    value_.string_value_.UnsafeSetDefault(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
  }
  value_.string_value_.SetNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(),
      ::std::string(value));
  // @@protoc_insertion_point(field_set_char:mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket.string_value)
}
inline void ConstantSidePacketCalculatorOptions_ConstantSidePacket::set_string_value(const char* value, size_t size) {
  if (!_internal_has_string_value()) {
    clear_value();
    set_has_string_value();
    value_.string_value_.UnsafeSetDefault(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
  }
  value_.string_value_.SetNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), ::std::string(
      reinterpret_cast<const char*>(value), size));
  // @@protoc_insertion_point(field_set_pointer:mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket.string_value)
}
inline std::string* ConstantSidePacketCalculatorOptions_ConstantSidePacket::_internal_mutable_string_value() {
  if (!_internal_has_string_value()) {
    clear_value();
    set_has_string_value();
    value_.string_value_.UnsafeSetDefault(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
  }
  return value_.string_value_.MutableNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
}
inline std::string* ConstantSidePacketCalculatorOptions_ConstantSidePacket::release_string_value() {
  // @@protoc_insertion_point(field_release:mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket.string_value)
  if (_internal_has_string_value()) {
    clear_has_value();
    return value_.string_value_.ReleaseNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
  } else {
    return nullptr;
  }
}
inline void ConstantSidePacketCalculatorOptions_ConstantSidePacket::set_allocated_string_value(std::string* string_value) {
  if (has_value()) {
    clear_value();
  }
  if (string_value != nullptr) {
    set_has_string_value();
    value_.string_value_.UnsafeSetDefault(string_value);
  }
  // @@protoc_insertion_point(field_set_allocated:mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket.string_value)
}

// optional uint64 uint64_value = 5;
inline bool ConstantSidePacketCalculatorOptions_ConstantSidePacket::_internal_has_uint64_value() const {
  return value_case() == kUint64Value;
}
inline bool ConstantSidePacketCalculatorOptions_ConstantSidePacket::has_uint64_value() const {
  return _internal_has_uint64_value();
}
inline void ConstantSidePacketCalculatorOptions_ConstantSidePacket::set_has_uint64_value() {
  _oneof_case_[0] = kUint64Value;
}
inline void ConstantSidePacketCalculatorOptions_ConstantSidePacket::clear_uint64_value() {
  if (_internal_has_uint64_value()) {
    value_.uint64_value_ = PROTOBUF_ULONGLONG(0);
    clear_has_value();
  }
}
inline ::PROTOBUF_NAMESPACE_ID::uint64 ConstantSidePacketCalculatorOptions_ConstantSidePacket::_internal_uint64_value() const {
  if (_internal_has_uint64_value()) {
    return value_.uint64_value_;
  }
  return PROTOBUF_ULONGLONG(0);
}
inline void ConstantSidePacketCalculatorOptions_ConstantSidePacket::_internal_set_uint64_value(::PROTOBUF_NAMESPACE_ID::uint64 value) {
  if (!_internal_has_uint64_value()) {
    clear_value();
    set_has_uint64_value();
  }
  value_.uint64_value_ = value;
}
inline ::PROTOBUF_NAMESPACE_ID::uint64 ConstantSidePacketCalculatorOptions_ConstantSidePacket::uint64_value() const {
  // @@protoc_insertion_point(field_get:mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket.uint64_value)
  return _internal_uint64_value();
}
inline void ConstantSidePacketCalculatorOptions_ConstantSidePacket::set_uint64_value(::PROTOBUF_NAMESPACE_ID::uint64 value) {
  _internal_set_uint64_value(value);
  // @@protoc_insertion_point(field_set:mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket.uint64_value)
}

// optional .mediapipe.ClassificationList classification_list_value = 6;
inline bool ConstantSidePacketCalculatorOptions_ConstantSidePacket::_internal_has_classification_list_value() const {
  return value_case() == kClassificationListValue;
}
inline bool ConstantSidePacketCalculatorOptions_ConstantSidePacket::has_classification_list_value() const {
  return _internal_has_classification_list_value();
}
inline void ConstantSidePacketCalculatorOptions_ConstantSidePacket::set_has_classification_list_value() {
  _oneof_case_[0] = kClassificationListValue;
}
inline ::mediapipe::ClassificationList* ConstantSidePacketCalculatorOptions_ConstantSidePacket::release_classification_list_value() {
  // @@protoc_insertion_point(field_release:mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket.classification_list_value)
  if (_internal_has_classification_list_value()) {
    clear_has_value();
      ::mediapipe::ClassificationList* temp = value_.classification_list_value_;
    value_.classification_list_value_ = nullptr;
    return temp;
  } else {
    return nullptr;
  }
}
inline const ::mediapipe::ClassificationList& ConstantSidePacketCalculatorOptions_ConstantSidePacket::_internal_classification_list_value() const {
  return _internal_has_classification_list_value()
      ? *value_.classification_list_value_
      : *reinterpret_cast< ::mediapipe::ClassificationList*>(&::mediapipe::_ClassificationList_default_instance_);
}
inline const ::mediapipe::ClassificationList& ConstantSidePacketCalculatorOptions_ConstantSidePacket::classification_list_value() const {
  // @@protoc_insertion_point(field_get:mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket.classification_list_value)
  return _internal_classification_list_value();
}
inline ::mediapipe::ClassificationList* ConstantSidePacketCalculatorOptions_ConstantSidePacket::_internal_mutable_classification_list_value() {
  if (!_internal_has_classification_list_value()) {
    clear_value();
    set_has_classification_list_value();
    value_.classification_list_value_ = CreateMaybeMessage< ::mediapipe::ClassificationList >(
        GetArenaNoVirtual());
  }
  return value_.classification_list_value_;
}
inline ::mediapipe::ClassificationList* ConstantSidePacketCalculatorOptions_ConstantSidePacket::mutable_classification_list_value() {
  // @@protoc_insertion_point(field_mutable:mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket.classification_list_value)
  return _internal_mutable_classification_list_value();
}

inline bool ConstantSidePacketCalculatorOptions_ConstantSidePacket::has_value() const {
  return value_case() != VALUE_NOT_SET;
}
inline void ConstantSidePacketCalculatorOptions_ConstantSidePacket::clear_has_value() {
  _oneof_case_[0] = VALUE_NOT_SET;
}
inline ConstantSidePacketCalculatorOptions_ConstantSidePacket::ValueCase ConstantSidePacketCalculatorOptions_ConstantSidePacket::value_case() const {
  return ConstantSidePacketCalculatorOptions_ConstantSidePacket::ValueCase(_oneof_case_[0]);
}
// -------------------------------------------------------------------

// ConstantSidePacketCalculatorOptions

// repeated .mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket packet = 1;
inline int ConstantSidePacketCalculatorOptions::_internal_packet_size() const {
  return packet_.size();
}
inline int ConstantSidePacketCalculatorOptions::packet_size() const {
  return _internal_packet_size();
}
inline void ConstantSidePacketCalculatorOptions::clear_packet() {
  packet_.Clear();
}
inline ::mediapipe::ConstantSidePacketCalculatorOptions_ConstantSidePacket* ConstantSidePacketCalculatorOptions::mutable_packet(int index) {
  // @@protoc_insertion_point(field_mutable:mediapipe.ConstantSidePacketCalculatorOptions.packet)
  return packet_.Mutable(index);
}
inline ::PROTOBUF_NAMESPACE_ID::RepeatedPtrField< ::mediapipe::ConstantSidePacketCalculatorOptions_ConstantSidePacket >*
ConstantSidePacketCalculatorOptions::mutable_packet() {
  // @@protoc_insertion_point(field_mutable_list:mediapipe.ConstantSidePacketCalculatorOptions.packet)
  return &packet_;
}
inline const ::mediapipe::ConstantSidePacketCalculatorOptions_ConstantSidePacket& ConstantSidePacketCalculatorOptions::_internal_packet(int index) const {
  return packet_.Get(index);
}
inline const ::mediapipe::ConstantSidePacketCalculatorOptions_ConstantSidePacket& ConstantSidePacketCalculatorOptions::packet(int index) const {
  // @@protoc_insertion_point(field_get:mediapipe.ConstantSidePacketCalculatorOptions.packet)
  return _internal_packet(index);
}
inline ::mediapipe::ConstantSidePacketCalculatorOptions_ConstantSidePacket* ConstantSidePacketCalculatorOptions::_internal_add_packet() {
  return packet_.Add();
}
inline ::mediapipe::ConstantSidePacketCalculatorOptions_ConstantSidePacket* ConstantSidePacketCalculatorOptions::add_packet() {
  // @@protoc_insertion_point(field_add:mediapipe.ConstantSidePacketCalculatorOptions.packet)
  return _internal_add_packet();
}
inline const ::PROTOBUF_NAMESPACE_ID::RepeatedPtrField< ::mediapipe::ConstantSidePacketCalculatorOptions_ConstantSidePacket >&
ConstantSidePacketCalculatorOptions::packet() const {
  // @@protoc_insertion_point(field_list:mediapipe.ConstantSidePacketCalculatorOptions.packet)
  return packet_;
}

#ifdef __GNUC__
  #pragma GCC diagnostic pop
#endif  // __GNUC__
// -------------------------------------------------------------------


// @@protoc_insertion_point(namespace_scope)

}  // namespace mediapipe

// @@protoc_insertion_point(global_scope)

#include <google/protobuf/port_undef.inc>
#endif  // GOOGLE_PROTOBUF_INCLUDED_GOOGLE_PROTOBUF_INCLUDED_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto
