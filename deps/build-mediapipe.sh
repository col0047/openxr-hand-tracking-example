#!/usr/bin/env bash

# Check if MEDIAPIPE_HOME is defined.
if [[ -z "${MEDIAPIPE_HOME}" ]]; then
  >&2 echo "MEDIAPIPE_HOME not set."
  exit 1
fi

(cd $MEDIAPIPE_HOME && bazel run --define MEDIAPIPE_DISABLE_GPU=1 mediapipe/examples/desktop/hand_tracking:hand_tracking_cpu)

exit 0
