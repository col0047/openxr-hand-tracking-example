// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: mediapipe/calculators/core/constant_side_packet_calculator.proto

#include "mediapipe/calculators/core/constant_side_packet_calculator.pb.h"

#include <algorithm>

#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/extension_set.h>
#include <google/protobuf/wire_format_lite.h>
#include <google/protobuf/descriptor.h>
#include <google/protobuf/generated_message_reflection.h>
#include <google/protobuf/reflection_ops.h>
#include <google/protobuf/wire_format.h>
// @@protoc_insertion_point(includes)
#include <google/protobuf/port_def.inc>
extern PROTOBUF_INTERNAL_EXPORT_mediapipe_2fframework_2fformats_2fclassification_2eproto ::PROTOBUF_NAMESPACE_ID::internal::SCCInfo<1> scc_info_ClassificationList_mediapipe_2fframework_2fformats_2fclassification_2eproto;
extern PROTOBUF_INTERNAL_EXPORT_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto ::PROTOBUF_NAMESPACE_ID::internal::SCCInfo<1> scc_info_ConstantSidePacketCalculatorOptions_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto;
extern PROTOBUF_INTERNAL_EXPORT_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto ::PROTOBUF_NAMESPACE_ID::internal::SCCInfo<1> scc_info_ConstantSidePacketCalculatorOptions_ConstantSidePacket_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto;
namespace mediapipe {
class ConstantSidePacketCalculatorOptions_ConstantSidePacketDefaultTypeInternal {
 public:
  ::PROTOBUF_NAMESPACE_ID::internal::ExplicitlyConstructed<ConstantSidePacketCalculatorOptions_ConstantSidePacket> _instance;
  ::PROTOBUF_NAMESPACE_ID::int32 int_value_;
  float float_value_;
  bool bool_value_;
  ::PROTOBUF_NAMESPACE_ID::internal::ArenaStringPtr string_value_;
  ::PROTOBUF_NAMESPACE_ID::uint64 uint64_value_;
  const ::mediapipe::ClassificationList* classification_list_value_;
} _ConstantSidePacketCalculatorOptions_ConstantSidePacket_default_instance_;
class ConstantSidePacketCalculatorOptionsDefaultTypeInternal {
 public:
  ::PROTOBUF_NAMESPACE_ID::internal::ExplicitlyConstructed<ConstantSidePacketCalculatorOptions> _instance;
} _ConstantSidePacketCalculatorOptions_default_instance_;
}  // namespace mediapipe
static void InitDefaultsscc_info_ConstantSidePacketCalculatorOptions_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto() {
  GOOGLE_PROTOBUF_VERIFY_VERSION;

  {
    void* ptr = &::mediapipe::_ConstantSidePacketCalculatorOptions_default_instance_;
    new (ptr) ::mediapipe::ConstantSidePacketCalculatorOptions();
    ::PROTOBUF_NAMESPACE_ID::internal::OnShutdownDestroyMessage(ptr);
  }
  ::mediapipe::ConstantSidePacketCalculatorOptions::InitAsDefaultInstance();
}

::PROTOBUF_NAMESPACE_ID::internal::SCCInfo<1> scc_info_ConstantSidePacketCalculatorOptions_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto =
    {{ATOMIC_VAR_INIT(::PROTOBUF_NAMESPACE_ID::internal::SCCInfoBase::kUninitialized), 1, 0, InitDefaultsscc_info_ConstantSidePacketCalculatorOptions_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto}, {
      &scc_info_ConstantSidePacketCalculatorOptions_ConstantSidePacket_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto.base,}};

static void InitDefaultsscc_info_ConstantSidePacketCalculatorOptions_ConstantSidePacket_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto() {
  GOOGLE_PROTOBUF_VERIFY_VERSION;

  {
    void* ptr = &::mediapipe::_ConstantSidePacketCalculatorOptions_ConstantSidePacket_default_instance_;
    new (ptr) ::mediapipe::ConstantSidePacketCalculatorOptions_ConstantSidePacket();
    ::PROTOBUF_NAMESPACE_ID::internal::OnShutdownDestroyMessage(ptr);
  }
  ::mediapipe::ConstantSidePacketCalculatorOptions_ConstantSidePacket::InitAsDefaultInstance();
}

::PROTOBUF_NAMESPACE_ID::internal::SCCInfo<1> scc_info_ConstantSidePacketCalculatorOptions_ConstantSidePacket_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto =
    {{ATOMIC_VAR_INIT(::PROTOBUF_NAMESPACE_ID::internal::SCCInfoBase::kUninitialized), 1, 0, InitDefaultsscc_info_ConstantSidePacketCalculatorOptions_ConstantSidePacket_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto}, {
      &scc_info_ClassificationList_mediapipe_2fframework_2fformats_2fclassification_2eproto.base,}};

static ::PROTOBUF_NAMESPACE_ID::Metadata file_level_metadata_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto[2];
static constexpr ::PROTOBUF_NAMESPACE_ID::EnumDescriptor const** file_level_enum_descriptors_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto = nullptr;
static constexpr ::PROTOBUF_NAMESPACE_ID::ServiceDescriptor const** file_level_service_descriptors_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto = nullptr;

const ::PROTOBUF_NAMESPACE_ID::uint32 TableStruct_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto::offsets[] PROTOBUF_SECTION_VARIABLE(protodesc_cold) = {
  PROTOBUF_FIELD_OFFSET(::mediapipe::ConstantSidePacketCalculatorOptions_ConstantSidePacket, _has_bits_),
  PROTOBUF_FIELD_OFFSET(::mediapipe::ConstantSidePacketCalculatorOptions_ConstantSidePacket, _internal_metadata_),
  ~0u,  // no _extensions_
  PROTOBUF_FIELD_OFFSET(::mediapipe::ConstantSidePacketCalculatorOptions_ConstantSidePacket, _oneof_case_[0]),
  ~0u,  // no _weak_field_map_
  offsetof(::mediapipe::ConstantSidePacketCalculatorOptions_ConstantSidePacketDefaultTypeInternal, int_value_),
  offsetof(::mediapipe::ConstantSidePacketCalculatorOptions_ConstantSidePacketDefaultTypeInternal, float_value_),
  offsetof(::mediapipe::ConstantSidePacketCalculatorOptions_ConstantSidePacketDefaultTypeInternal, bool_value_),
  offsetof(::mediapipe::ConstantSidePacketCalculatorOptions_ConstantSidePacketDefaultTypeInternal, string_value_),
  offsetof(::mediapipe::ConstantSidePacketCalculatorOptions_ConstantSidePacketDefaultTypeInternal, uint64_value_),
  offsetof(::mediapipe::ConstantSidePacketCalculatorOptions_ConstantSidePacketDefaultTypeInternal, classification_list_value_),
  PROTOBUF_FIELD_OFFSET(::mediapipe::ConstantSidePacketCalculatorOptions_ConstantSidePacket, value_),
  ~0u,
  ~0u,
  ~0u,
  ~0u,
  ~0u,
  ~0u,
  PROTOBUF_FIELD_OFFSET(::mediapipe::ConstantSidePacketCalculatorOptions, _has_bits_),
  PROTOBUF_FIELD_OFFSET(::mediapipe::ConstantSidePacketCalculatorOptions, _internal_metadata_),
  ~0u,  // no _extensions_
  ~0u,  // no _oneof_case_
  ~0u,  // no _weak_field_map_
  PROTOBUF_FIELD_OFFSET(::mediapipe::ConstantSidePacketCalculatorOptions, packet_),
  ~0u,
};
static const ::PROTOBUF_NAMESPACE_ID::internal::MigrationSchema schemas[] PROTOBUF_SECTION_VARIABLE(protodesc_cold) = {
  { 0, 12, sizeof(::mediapipe::ConstantSidePacketCalculatorOptions_ConstantSidePacket)},
  { 18, 24, sizeof(::mediapipe::ConstantSidePacketCalculatorOptions)},
};

static ::PROTOBUF_NAMESPACE_ID::Message const * const file_default_instances[] = {
  reinterpret_cast<const ::PROTOBUF_NAMESPACE_ID::Message*>(&::mediapipe::_ConstantSidePacketCalculatorOptions_ConstantSidePacket_default_instance_),
  reinterpret_cast<const ::PROTOBUF_NAMESPACE_ID::Message*>(&::mediapipe::_ConstantSidePacketCalculatorOptions_default_instance_),
};

const char descriptor_table_protodef_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto[] PROTOBUF_SECTION_VARIABLE(protodesc_cold) =
  "\n@mediapipe/calculators/core/constant_si"
  "de_packet_calculator.proto\022\tmediapipe\032$m"
  "ediapipe/framework/calculator.proto\0320med"
  "iapipe/framework/formats/classification."
  "proto\"\255\003\n#ConstantSidePacketCalculatorOp"
  "tions\022Q\n\006packet\030\001 \003(\0132A.mediapipe.Consta"
  "ntSidePacketCalculatorOptions.ConstantSi"
  "dePacket\032\323\001\n\022ConstantSidePacket\022\023\n\tint_v"
  "alue\030\001 \001(\005H\000\022\025\n\013float_value\030\002 \001(\002H\000\022\024\n\nb"
  "ool_value\030\003 \001(\010H\000\022\026\n\014string_value\030\004 \001(\tH"
  "\000\022\026\n\014uint64_value\030\005 \001(\004H\000\022B\n\031classificat"
  "ion_list_value\030\006 \001(\0132\035.mediapipe.Classif"
  "icationListH\000B\007\n\005value2]\n\003ext\022\034.mediapip"
  "e.CalculatorOptions\030\205\252\356\212\001 \001(\0132..mediapip"
  "e.ConstantSidePacketCalculatorOptionsB\014\242"
  "\002\tMediaPipe"
  ;
static const ::PROTOBUF_NAMESPACE_ID::internal::DescriptorTable*const descriptor_table_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto_deps[2] = {
  &::descriptor_table_mediapipe_2fframework_2fcalculator_2eproto,
  &::descriptor_table_mediapipe_2fframework_2fformats_2fclassification_2eproto,
};
static ::PROTOBUF_NAMESPACE_ID::internal::SCCInfoBase*const descriptor_table_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto_sccs[2] = {
  &scc_info_ConstantSidePacketCalculatorOptions_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto.base,
  &scc_info_ConstantSidePacketCalculatorOptions_ConstantSidePacket_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto.base,
};
static ::PROTOBUF_NAMESPACE_ID::internal::once_flag descriptor_table_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto_once;
static bool descriptor_table_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto_initialized = false;
const ::PROTOBUF_NAMESPACE_ID::internal::DescriptorTable descriptor_table_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto = {
  &descriptor_table_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto_initialized, descriptor_table_protodef_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto, "mediapipe/calculators/core/constant_side_packet_calculator.proto", 611,
  &descriptor_table_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto_once, descriptor_table_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto_sccs, descriptor_table_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto_deps, 2, 2,
  schemas, file_default_instances, TableStruct_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto::offsets,
  file_level_metadata_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto, 2, file_level_enum_descriptors_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto, file_level_service_descriptors_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto,
};

// Force running AddDescriptors() at dynamic initialization time.
static bool dynamic_init_dummy_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto = (  ::PROTOBUF_NAMESPACE_ID::internal::AddDescriptors(&descriptor_table_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto), true);
namespace mediapipe {

// ===================================================================

void ConstantSidePacketCalculatorOptions_ConstantSidePacket::InitAsDefaultInstance() {
  ::mediapipe::_ConstantSidePacketCalculatorOptions_ConstantSidePacket_default_instance_.int_value_ = 0;
  ::mediapipe::_ConstantSidePacketCalculatorOptions_ConstantSidePacket_default_instance_.float_value_ = 0;
  ::mediapipe::_ConstantSidePacketCalculatorOptions_ConstantSidePacket_default_instance_.bool_value_ = false;
  ::mediapipe::_ConstantSidePacketCalculatorOptions_ConstantSidePacket_default_instance_.string_value_.UnsafeSetDefault(
      &::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
  ::mediapipe::_ConstantSidePacketCalculatorOptions_ConstantSidePacket_default_instance_.uint64_value_ = PROTOBUF_ULONGLONG(0);
  ::mediapipe::_ConstantSidePacketCalculatorOptions_ConstantSidePacket_default_instance_.classification_list_value_ = const_cast< ::mediapipe::ClassificationList*>(
      ::mediapipe::ClassificationList::internal_default_instance());
}
class ConstantSidePacketCalculatorOptions_ConstantSidePacket::_Internal {
 public:
  using HasBits = decltype(std::declval<ConstantSidePacketCalculatorOptions_ConstantSidePacket>()._has_bits_);
  static const ::mediapipe::ClassificationList& classification_list_value(const ConstantSidePacketCalculatorOptions_ConstantSidePacket* msg);
};

const ::mediapipe::ClassificationList&
ConstantSidePacketCalculatorOptions_ConstantSidePacket::_Internal::classification_list_value(const ConstantSidePacketCalculatorOptions_ConstantSidePacket* msg) {
  return *msg->value_.classification_list_value_;
}
void ConstantSidePacketCalculatorOptions_ConstantSidePacket::set_allocated_classification_list_value(::mediapipe::ClassificationList* classification_list_value) {
  ::PROTOBUF_NAMESPACE_ID::Arena* message_arena = GetArenaNoVirtual();
  clear_value();
  if (classification_list_value) {
    ::PROTOBUF_NAMESPACE_ID::Arena* submessage_arena = nullptr;
    if (message_arena != submessage_arena) {
      classification_list_value = ::PROTOBUF_NAMESPACE_ID::internal::GetOwnedMessage(
          message_arena, classification_list_value, submessage_arena);
    }
    set_has_classification_list_value();
    value_.classification_list_value_ = classification_list_value;
  }
  // @@protoc_insertion_point(field_set_allocated:mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket.classification_list_value)
}
void ConstantSidePacketCalculatorOptions_ConstantSidePacket::clear_classification_list_value() {
  if (_internal_has_classification_list_value()) {
    delete value_.classification_list_value_;
    clear_has_value();
  }
}
ConstantSidePacketCalculatorOptions_ConstantSidePacket::ConstantSidePacketCalculatorOptions_ConstantSidePacket()
  : ::PROTOBUF_NAMESPACE_ID::Message(), _internal_metadata_(nullptr) {
  SharedCtor();
  // @@protoc_insertion_point(constructor:mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket)
}
ConstantSidePacketCalculatorOptions_ConstantSidePacket::ConstantSidePacketCalculatorOptions_ConstantSidePacket(const ConstantSidePacketCalculatorOptions_ConstantSidePacket& from)
  : ::PROTOBUF_NAMESPACE_ID::Message(),
      _internal_metadata_(nullptr),
      _has_bits_(from._has_bits_) {
  _internal_metadata_.MergeFrom(from._internal_metadata_);
  clear_has_value();
  switch (from.value_case()) {
    case kIntValue: {
      _internal_set_int_value(from._internal_int_value());
      break;
    }
    case kFloatValue: {
      _internal_set_float_value(from._internal_float_value());
      break;
    }
    case kBoolValue: {
      _internal_set_bool_value(from._internal_bool_value());
      break;
    }
    case kStringValue: {
      _internal_set_string_value(from._internal_string_value());
      break;
    }
    case kUint64Value: {
      _internal_set_uint64_value(from._internal_uint64_value());
      break;
    }
    case kClassificationListValue: {
      _internal_mutable_classification_list_value()->::mediapipe::ClassificationList::MergeFrom(from._internal_classification_list_value());
      break;
    }
    case VALUE_NOT_SET: {
      break;
    }
  }
  // @@protoc_insertion_point(copy_constructor:mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket)
}

void ConstantSidePacketCalculatorOptions_ConstantSidePacket::SharedCtor() {
  ::PROTOBUF_NAMESPACE_ID::internal::InitSCC(&scc_info_ConstantSidePacketCalculatorOptions_ConstantSidePacket_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto.base);
  clear_has_value();
}

ConstantSidePacketCalculatorOptions_ConstantSidePacket::~ConstantSidePacketCalculatorOptions_ConstantSidePacket() {
  // @@protoc_insertion_point(destructor:mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket)
  SharedDtor();
}

void ConstantSidePacketCalculatorOptions_ConstantSidePacket::SharedDtor() {
  if (has_value()) {
    clear_value();
  }
}

void ConstantSidePacketCalculatorOptions_ConstantSidePacket::SetCachedSize(int size) const {
  _cached_size_.Set(size);
}
const ConstantSidePacketCalculatorOptions_ConstantSidePacket& ConstantSidePacketCalculatorOptions_ConstantSidePacket::default_instance() {
  ::PROTOBUF_NAMESPACE_ID::internal::InitSCC(&::scc_info_ConstantSidePacketCalculatorOptions_ConstantSidePacket_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto.base);
  return *internal_default_instance();
}


void ConstantSidePacketCalculatorOptions_ConstantSidePacket::clear_value() {
// @@protoc_insertion_point(one_of_clear_start:mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket)
  switch (value_case()) {
    case kIntValue: {
      // No need to clear
      break;
    }
    case kFloatValue: {
      // No need to clear
      break;
    }
    case kBoolValue: {
      // No need to clear
      break;
    }
    case kStringValue: {
      value_.string_value_.DestroyNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
      break;
    }
    case kUint64Value: {
      // No need to clear
      break;
    }
    case kClassificationListValue: {
      delete value_.classification_list_value_;
      break;
    }
    case VALUE_NOT_SET: {
      break;
    }
  }
  _oneof_case_[0] = VALUE_NOT_SET;
}


void ConstantSidePacketCalculatorOptions_ConstantSidePacket::Clear() {
// @@protoc_insertion_point(message_clear_start:mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket)
  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  // Prevent compiler warnings about cached_has_bits being unused
  (void) cached_has_bits;

  clear_value();
  _has_bits_.Clear();
  _internal_metadata_.Clear();
}

const char* ConstantSidePacketCalculatorOptions_ConstantSidePacket::_InternalParse(const char* ptr, ::PROTOBUF_NAMESPACE_ID::internal::ParseContext* ctx) {
#define CHK_(x) if (PROTOBUF_PREDICT_FALSE(!(x))) goto failure
  while (!ctx->Done(&ptr)) {
    ::PROTOBUF_NAMESPACE_ID::uint32 tag;
    ptr = ::PROTOBUF_NAMESPACE_ID::internal::ReadTag(ptr, &tag);
    CHK_(ptr);
    switch (tag >> 3) {
      // optional int32 int_value = 1;
      case 1:
        if (PROTOBUF_PREDICT_TRUE(static_cast<::PROTOBUF_NAMESPACE_ID::uint8>(tag) == 8)) {
          _internal_set_int_value(::PROTOBUF_NAMESPACE_ID::internal::ReadVarint(&ptr));
          CHK_(ptr);
        } else goto handle_unusual;
        continue;
      // optional float float_value = 2;
      case 2:
        if (PROTOBUF_PREDICT_TRUE(static_cast<::PROTOBUF_NAMESPACE_ID::uint8>(tag) == 21)) {
          _internal_set_float_value(::PROTOBUF_NAMESPACE_ID::internal::UnalignedLoad<float>(ptr));
          ptr += sizeof(float);
        } else goto handle_unusual;
        continue;
      // optional bool bool_value = 3;
      case 3:
        if (PROTOBUF_PREDICT_TRUE(static_cast<::PROTOBUF_NAMESPACE_ID::uint8>(tag) == 24)) {
          _internal_set_bool_value(::PROTOBUF_NAMESPACE_ID::internal::ReadVarint(&ptr));
          CHK_(ptr);
        } else goto handle_unusual;
        continue;
      // optional string string_value = 4;
      case 4:
        if (PROTOBUF_PREDICT_TRUE(static_cast<::PROTOBUF_NAMESPACE_ID::uint8>(tag) == 34)) {
          auto str = _internal_mutable_string_value();
          ptr = ::PROTOBUF_NAMESPACE_ID::internal::InlineGreedyStringParser(str, ptr, ctx);
          #ifndef NDEBUG
          ::PROTOBUF_NAMESPACE_ID::internal::VerifyUTF8(str, "mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket.string_value");
          #endif  // !NDEBUG
          CHK_(ptr);
        } else goto handle_unusual;
        continue;
      // optional uint64 uint64_value = 5;
      case 5:
        if (PROTOBUF_PREDICT_TRUE(static_cast<::PROTOBUF_NAMESPACE_ID::uint8>(tag) == 40)) {
          _internal_set_uint64_value(::PROTOBUF_NAMESPACE_ID::internal::ReadVarint(&ptr));
          CHK_(ptr);
        } else goto handle_unusual;
        continue;
      // optional .mediapipe.ClassificationList classification_list_value = 6;
      case 6:
        if (PROTOBUF_PREDICT_TRUE(static_cast<::PROTOBUF_NAMESPACE_ID::uint8>(tag) == 50)) {
          ptr = ctx->ParseMessage(_internal_mutable_classification_list_value(), ptr);
          CHK_(ptr);
        } else goto handle_unusual;
        continue;
      default: {
      handle_unusual:
        if ((tag & 7) == 4 || tag == 0) {
          ctx->SetLastTag(tag);
          goto success;
        }
        ptr = UnknownFieldParse(tag, &_internal_metadata_, ptr, ctx);
        CHK_(ptr != nullptr);
        continue;
      }
    }  // switch
  }  // while
success:
  return ptr;
failure:
  ptr = nullptr;
  goto success;
#undef CHK_
}

::PROTOBUF_NAMESPACE_ID::uint8* ConstantSidePacketCalculatorOptions_ConstantSidePacket::_InternalSerialize(
    ::PROTOBUF_NAMESPACE_ID::uint8* target, ::PROTOBUF_NAMESPACE_ID::io::EpsCopyOutputStream* stream) const {
  // @@protoc_insertion_point(serialize_to_array_start:mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket)
  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  (void) cached_has_bits;

  switch (value_case()) {
    case kIntValue: {
      target = stream->EnsureSpace(target);
      target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::WriteInt32ToArray(1, this->_internal_int_value(), target);
      break;
    }
    case kFloatValue: {
      target = stream->EnsureSpace(target);
      target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::WriteFloatToArray(2, this->_internal_float_value(), target);
      break;
    }
    case kBoolValue: {
      target = stream->EnsureSpace(target);
      target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::WriteBoolToArray(3, this->_internal_bool_value(), target);
      break;
    }
    case kStringValue: {
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormat::VerifyUTF8StringNamedField(
        this->_internal_string_value().data(), static_cast<int>(this->_internal_string_value().length()),
        ::PROTOBUF_NAMESPACE_ID::internal::WireFormat::SERIALIZE,
        "mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket.string_value");
      target = stream->WriteStringMaybeAliased(
          4, this->_internal_string_value(), target);
      break;
    }
    case kUint64Value: {
      target = stream->EnsureSpace(target);
      target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::WriteUInt64ToArray(5, this->_internal_uint64_value(), target);
      break;
    }
    case kClassificationListValue: {
      target = stream->EnsureSpace(target);
      target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::
        InternalWriteMessage(
          6, _Internal::classification_list_value(this), target, stream);
      break;
    }
    default: ;
  }
  if (PROTOBUF_PREDICT_FALSE(_internal_metadata_.have_unknown_fields())) {
    target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormat::InternalSerializeUnknownFieldsToArray(
        _internal_metadata_.unknown_fields(), target, stream);
  }
  // @@protoc_insertion_point(serialize_to_array_end:mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket)
  return target;
}

size_t ConstantSidePacketCalculatorOptions_ConstantSidePacket::ByteSizeLong() const {
// @@protoc_insertion_point(message_byte_size_start:mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket)
  size_t total_size = 0;

  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  // Prevent compiler warnings about cached_has_bits being unused
  (void) cached_has_bits;

  switch (value_case()) {
    // optional int32 int_value = 1;
    case kIntValue: {
      total_size += 1 +
        ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::Int32Size(
          this->_internal_int_value());
      break;
    }
    // optional float float_value = 2;
    case kFloatValue: {
      total_size += 1 + 4;
      break;
    }
    // optional bool bool_value = 3;
    case kBoolValue: {
      total_size += 1 + 1;
      break;
    }
    // optional string string_value = 4;
    case kStringValue: {
      total_size += 1 +
        ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::StringSize(
          this->_internal_string_value());
      break;
    }
    // optional uint64 uint64_value = 5;
    case kUint64Value: {
      total_size += 1 +
        ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::UInt64Size(
          this->_internal_uint64_value());
      break;
    }
    // optional .mediapipe.ClassificationList classification_list_value = 6;
    case kClassificationListValue: {
      total_size += 1 +
        ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::MessageSize(
          *value_.classification_list_value_);
      break;
    }
    case VALUE_NOT_SET: {
      break;
    }
  }
  if (PROTOBUF_PREDICT_FALSE(_internal_metadata_.have_unknown_fields())) {
    return ::PROTOBUF_NAMESPACE_ID::internal::ComputeUnknownFieldsSize(
        _internal_metadata_, total_size, &_cached_size_);
  }
  int cached_size = ::PROTOBUF_NAMESPACE_ID::internal::ToCachedSize(total_size);
  SetCachedSize(cached_size);
  return total_size;
}

void ConstantSidePacketCalculatorOptions_ConstantSidePacket::MergeFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) {
// @@protoc_insertion_point(generalized_merge_from_start:mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket)
  GOOGLE_DCHECK_NE(&from, this);
  const ConstantSidePacketCalculatorOptions_ConstantSidePacket* source =
      ::PROTOBUF_NAMESPACE_ID::DynamicCastToGenerated<ConstantSidePacketCalculatorOptions_ConstantSidePacket>(
          &from);
  if (source == nullptr) {
  // @@protoc_insertion_point(generalized_merge_from_cast_fail:mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket)
    ::PROTOBUF_NAMESPACE_ID::internal::ReflectionOps::Merge(from, this);
  } else {
  // @@protoc_insertion_point(generalized_merge_from_cast_success:mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket)
    MergeFrom(*source);
  }
}

void ConstantSidePacketCalculatorOptions_ConstantSidePacket::MergeFrom(const ConstantSidePacketCalculatorOptions_ConstantSidePacket& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket)
  GOOGLE_DCHECK_NE(&from, this);
  _internal_metadata_.MergeFrom(from._internal_metadata_);
  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  (void) cached_has_bits;

  switch (from.value_case()) {
    case kIntValue: {
      _internal_set_int_value(from._internal_int_value());
      break;
    }
    case kFloatValue: {
      _internal_set_float_value(from._internal_float_value());
      break;
    }
    case kBoolValue: {
      _internal_set_bool_value(from._internal_bool_value());
      break;
    }
    case kStringValue: {
      _internal_set_string_value(from._internal_string_value());
      break;
    }
    case kUint64Value: {
      _internal_set_uint64_value(from._internal_uint64_value());
      break;
    }
    case kClassificationListValue: {
      _internal_mutable_classification_list_value()->::mediapipe::ClassificationList::MergeFrom(from._internal_classification_list_value());
      break;
    }
    case VALUE_NOT_SET: {
      break;
    }
  }
}

void ConstantSidePacketCalculatorOptions_ConstantSidePacket::CopyFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) {
// @@protoc_insertion_point(generalized_copy_from_start:mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

void ConstantSidePacketCalculatorOptions_ConstantSidePacket::CopyFrom(const ConstantSidePacketCalculatorOptions_ConstantSidePacket& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool ConstantSidePacketCalculatorOptions_ConstantSidePacket::IsInitialized() const {
  return true;
}

void ConstantSidePacketCalculatorOptions_ConstantSidePacket::InternalSwap(ConstantSidePacketCalculatorOptions_ConstantSidePacket* other) {
  using std::swap;
  _internal_metadata_.Swap(&other->_internal_metadata_);
  swap(_has_bits_[0], other->_has_bits_[0]);
  swap(value_, other->value_);
  swap(_oneof_case_[0], other->_oneof_case_[0]);
}

::PROTOBUF_NAMESPACE_ID::Metadata ConstantSidePacketCalculatorOptions_ConstantSidePacket::GetMetadata() const {
  return GetMetadataStatic();
}


// ===================================================================

void ConstantSidePacketCalculatorOptions::InitAsDefaultInstance() {
}
class ConstantSidePacketCalculatorOptions::_Internal {
 public:
  using HasBits = decltype(std::declval<ConstantSidePacketCalculatorOptions>()._has_bits_);
};

ConstantSidePacketCalculatorOptions::ConstantSidePacketCalculatorOptions()
  : ::PROTOBUF_NAMESPACE_ID::Message(), _internal_metadata_(nullptr) {
  SharedCtor();
  // @@protoc_insertion_point(constructor:mediapipe.ConstantSidePacketCalculatorOptions)
}
ConstantSidePacketCalculatorOptions::ConstantSidePacketCalculatorOptions(const ConstantSidePacketCalculatorOptions& from)
  : ::PROTOBUF_NAMESPACE_ID::Message(),
      _internal_metadata_(nullptr),
      _has_bits_(from._has_bits_),
      packet_(from.packet_) {
  _internal_metadata_.MergeFrom(from._internal_metadata_);
  // @@protoc_insertion_point(copy_constructor:mediapipe.ConstantSidePacketCalculatorOptions)
}

void ConstantSidePacketCalculatorOptions::SharedCtor() {
  ::PROTOBUF_NAMESPACE_ID::internal::InitSCC(&scc_info_ConstantSidePacketCalculatorOptions_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto.base);
}

ConstantSidePacketCalculatorOptions::~ConstantSidePacketCalculatorOptions() {
  // @@protoc_insertion_point(destructor:mediapipe.ConstantSidePacketCalculatorOptions)
  SharedDtor();
}

void ConstantSidePacketCalculatorOptions::SharedDtor() {
}

void ConstantSidePacketCalculatorOptions::SetCachedSize(int size) const {
  _cached_size_.Set(size);
}
const ConstantSidePacketCalculatorOptions& ConstantSidePacketCalculatorOptions::default_instance() {
  ::PROTOBUF_NAMESPACE_ID::internal::InitSCC(&::scc_info_ConstantSidePacketCalculatorOptions_mediapipe_2fcalculators_2fcore_2fconstant_5fside_5fpacket_5fcalculator_2eproto.base);
  return *internal_default_instance();
}


void ConstantSidePacketCalculatorOptions::Clear() {
// @@protoc_insertion_point(message_clear_start:mediapipe.ConstantSidePacketCalculatorOptions)
  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  // Prevent compiler warnings about cached_has_bits being unused
  (void) cached_has_bits;

  packet_.Clear();
  _has_bits_.Clear();
  _internal_metadata_.Clear();
}

const char* ConstantSidePacketCalculatorOptions::_InternalParse(const char* ptr, ::PROTOBUF_NAMESPACE_ID::internal::ParseContext* ctx) {
#define CHK_(x) if (PROTOBUF_PREDICT_FALSE(!(x))) goto failure
  while (!ctx->Done(&ptr)) {
    ::PROTOBUF_NAMESPACE_ID::uint32 tag;
    ptr = ::PROTOBUF_NAMESPACE_ID::internal::ReadTag(ptr, &tag);
    CHK_(ptr);
    switch (tag >> 3) {
      // repeated .mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket packet = 1;
      case 1:
        if (PROTOBUF_PREDICT_TRUE(static_cast<::PROTOBUF_NAMESPACE_ID::uint8>(tag) == 10)) {
          ptr -= 1;
          do {
            ptr += 1;
            ptr = ctx->ParseMessage(_internal_add_packet(), ptr);
            CHK_(ptr);
            if (!ctx->DataAvailable(ptr)) break;
          } while (::PROTOBUF_NAMESPACE_ID::internal::ExpectTag<10>(ptr));
        } else goto handle_unusual;
        continue;
      default: {
      handle_unusual:
        if ((tag & 7) == 4 || tag == 0) {
          ctx->SetLastTag(tag);
          goto success;
        }
        ptr = UnknownFieldParse(tag, &_internal_metadata_, ptr, ctx);
        CHK_(ptr != nullptr);
        continue;
      }
    }  // switch
  }  // while
success:
  return ptr;
failure:
  ptr = nullptr;
  goto success;
#undef CHK_
}

::PROTOBUF_NAMESPACE_ID::uint8* ConstantSidePacketCalculatorOptions::_InternalSerialize(
    ::PROTOBUF_NAMESPACE_ID::uint8* target, ::PROTOBUF_NAMESPACE_ID::io::EpsCopyOutputStream* stream) const {
  // @@protoc_insertion_point(serialize_to_array_start:mediapipe.ConstantSidePacketCalculatorOptions)
  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  (void) cached_has_bits;

  // repeated .mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket packet = 1;
  for (unsigned int i = 0,
      n = static_cast<unsigned int>(this->_internal_packet_size()); i < n; i++) {
    target = stream->EnsureSpace(target);
    target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::
      InternalWriteMessage(1, this->_internal_packet(i), target, stream);
  }

  if (PROTOBUF_PREDICT_FALSE(_internal_metadata_.have_unknown_fields())) {
    target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormat::InternalSerializeUnknownFieldsToArray(
        _internal_metadata_.unknown_fields(), target, stream);
  }
  // @@protoc_insertion_point(serialize_to_array_end:mediapipe.ConstantSidePacketCalculatorOptions)
  return target;
}

size_t ConstantSidePacketCalculatorOptions::ByteSizeLong() const {
// @@protoc_insertion_point(message_byte_size_start:mediapipe.ConstantSidePacketCalculatorOptions)
  size_t total_size = 0;

  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  // Prevent compiler warnings about cached_has_bits being unused
  (void) cached_has_bits;

  // repeated .mediapipe.ConstantSidePacketCalculatorOptions.ConstantSidePacket packet = 1;
  total_size += 1UL * this->_internal_packet_size();
  for (const auto& msg : this->packet_) {
    total_size +=
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::MessageSize(msg);
  }

  if (PROTOBUF_PREDICT_FALSE(_internal_metadata_.have_unknown_fields())) {
    return ::PROTOBUF_NAMESPACE_ID::internal::ComputeUnknownFieldsSize(
        _internal_metadata_, total_size, &_cached_size_);
  }
  int cached_size = ::PROTOBUF_NAMESPACE_ID::internal::ToCachedSize(total_size);
  SetCachedSize(cached_size);
  return total_size;
}

void ConstantSidePacketCalculatorOptions::MergeFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) {
// @@protoc_insertion_point(generalized_merge_from_start:mediapipe.ConstantSidePacketCalculatorOptions)
  GOOGLE_DCHECK_NE(&from, this);
  const ConstantSidePacketCalculatorOptions* source =
      ::PROTOBUF_NAMESPACE_ID::DynamicCastToGenerated<ConstantSidePacketCalculatorOptions>(
          &from);
  if (source == nullptr) {
  // @@protoc_insertion_point(generalized_merge_from_cast_fail:mediapipe.ConstantSidePacketCalculatorOptions)
    ::PROTOBUF_NAMESPACE_ID::internal::ReflectionOps::Merge(from, this);
  } else {
  // @@protoc_insertion_point(generalized_merge_from_cast_success:mediapipe.ConstantSidePacketCalculatorOptions)
    MergeFrom(*source);
  }
}

void ConstantSidePacketCalculatorOptions::MergeFrom(const ConstantSidePacketCalculatorOptions& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:mediapipe.ConstantSidePacketCalculatorOptions)
  GOOGLE_DCHECK_NE(&from, this);
  _internal_metadata_.MergeFrom(from._internal_metadata_);
  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  (void) cached_has_bits;

  packet_.MergeFrom(from.packet_);
}

void ConstantSidePacketCalculatorOptions::CopyFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) {
// @@protoc_insertion_point(generalized_copy_from_start:mediapipe.ConstantSidePacketCalculatorOptions)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

void ConstantSidePacketCalculatorOptions::CopyFrom(const ConstantSidePacketCalculatorOptions& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:mediapipe.ConstantSidePacketCalculatorOptions)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool ConstantSidePacketCalculatorOptions::IsInitialized() const {
  return true;
}

void ConstantSidePacketCalculatorOptions::InternalSwap(ConstantSidePacketCalculatorOptions* other) {
  using std::swap;
  _internal_metadata_.Swap(&other->_internal_metadata_);
  swap(_has_bits_[0], other->_has_bits_[0]);
  packet_.InternalSwap(&other->packet_);
}

::PROTOBUF_NAMESPACE_ID::Metadata ConstantSidePacketCalculatorOptions::GetMetadata() const {
  return GetMetadataStatic();
}

#if !defined(_MSC_VER) || _MSC_VER >= 1900
const int ConstantSidePacketCalculatorOptions::kExtFieldNumber;
#endif
::PROTOBUF_NAMESPACE_ID::internal::ExtensionIdentifier< ::mediapipe::CalculatorOptions,
    ::PROTOBUF_NAMESPACE_ID::internal::MessageTypeTraits< ::mediapipe::ConstantSidePacketCalculatorOptions >, 11, false >
  ConstantSidePacketCalculatorOptions::ext(kExtFieldNumber, *::mediapipe::ConstantSidePacketCalculatorOptions::internal_default_instance());

// @@protoc_insertion_point(namespace_scope)
}  // namespace mediapipe
PROTOBUF_NAMESPACE_OPEN
template<> PROTOBUF_NOINLINE ::mediapipe::ConstantSidePacketCalculatorOptions_ConstantSidePacket* Arena::CreateMaybeMessage< ::mediapipe::ConstantSidePacketCalculatorOptions_ConstantSidePacket >(Arena* arena) {
  return Arena::CreateInternal< ::mediapipe::ConstantSidePacketCalculatorOptions_ConstantSidePacket >(arena);
}
template<> PROTOBUF_NOINLINE ::mediapipe::ConstantSidePacketCalculatorOptions* Arena::CreateMaybeMessage< ::mediapipe::ConstantSidePacketCalculatorOptions >(Arena* arena) {
  return Arena::CreateInternal< ::mediapipe::ConstantSidePacketCalculatorOptions >(arena);
}
PROTOBUF_NAMESPACE_CLOSE

// @@protoc_insertion_point(global_scope)
#include <google/protobuf/port_undef.inc>
