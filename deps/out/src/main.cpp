#include <cstdlib>

#include "mediapipe/framework/calculator_framework.h"
#include "mediapipe/framework/formats/image_frame.h"
#include "mediapipe/framework/formats/image_frame_opencv.h"
#include "mediapipe/framework/port/file_helpers.h"
#include "mediapipe/framework/port/opencv_highgui_inc.h"
#include "mediapipe/framework/port/opencv_imgproc_inc.h"
#include "mediapipe/framework/port/opencv_video_inc.h"
#include "mediapipe/framework/port/parse_text_proto.h"
#include "mediapipe/framework/port/status.h"
#include "mediapipe/framework/formats/landmark.pb.h"

constexpr char kInputStream[] = "input_video";
constexpr char kOutputStream[] = "output_video";
constexpr char kWindowName[] = "Hand-Tracking - Monado XR - Debug";
constexpr char kMultiHandLandmarksOutputStream[] = "landmarks";

void RunMPPGraph() {
  std::string calculator_graph_config_contents;

  MP_RETURN_IF_ERROR(mediapipe::file::GetContents(
      "../deps/mediapipe/mediapipe/graphs/hand_tracking/hand_tracking_desktop_live.pbtxt",
      &calculator_graph_config_contents));

  std::cout << "Get calculator graph config contents: "
            << calculator_graph_config_contents << std::endl;
  mediapipe::CalculatorGraphConfig config =
      mediapipe::ParseTextProtoOrDie<mediapipe::CalculatorGraphConfig>(
          calculator_graph_config_contents);

  std::cout << "Initialize the calculator graph.\n";
  mediapipe::CalculatorGraph graph;
  MP_RETURN_IF_ERROR(graph.Initialize(config));

  std::cout << "Initialize the camera or load the video.\n";
  cv::VideoCapture capture;
  const bool load_video = false;
  capture.open(0);
  RET_CHECK(capture.isOpened());


  std::cout << "Start running the calculator graph.\n";
  ASSIGN_OR_RETURN(mediapipe::OutputStreamPoller poller,
                   graph.AddOutputStreamPoller(kOutputStream));
  ASSIGN_OR_RETURN(mediapipe::OutputStreamPoller multi_hand_landmarks_poller,
                   graph.AddOutputStreamPoller(kMultiHandLandmarksOutputStream));
  MP_RETURN_IF_ERROR(graph.StartRun({}));

  std::cout << "Start grabbing and processing frames.\n";
  bool grab_frames = true;
  while (grab_frames) {
    // Capture opencv camera or video frame.
    cv::Mat camera_frame_raw;
    capture >> camera_frame_raw;
    if (camera_frame_raw.empty()) {
      if (!load_video) {
        std::cout << "Ignore empty frames from camera\n.";
        continue;
      }
      std::cout << "Empty frame, end of video reached\n.";
      break;
    }
    cv::Mat camera_frame;
    cv::cvtColor(camera_frame_raw, camera_frame, cv::COLOR_BGR2RGB);
    if (!load_video) {
      cv::flip(camera_frame, camera_frame, /*flipcode=HORIZONTAL*/ 1);
    }

    // Wrap Mat into an ImageFrame.
    auto input_frame = absl::make_unique<mediapipe::ImageFrame>(
        mediapipe::ImageFormat::SRGB, camera_frame.cols, camera_frame.rows,
        mediapipe::ImageFrame::kDefaultAlignmentBoundary);
    cv::Mat input_frame_mat = mediapipe::formats::MatView(input_frame.get());
    camera_frame.copyTo(input_frame_mat);

    // Send image packet into the graph.
    size_t frame_timestamp_us =
        (double)cv::getTickCount() / (double)cv::getTickFrequency() * 1e6;
    MP_RETURN_IF_ERROR(graph.AddPacketToInputStream(
        kInputStream, mediapipe::Adopt(input_frame.release())
                          .At(mediapipe::Timestamp(frame_timestamp_us))));

    // Get the graph result packet, or stop if that fails.
    mediapipe::Packet packet;
    if (!poller.Next(&packet)) break;
    auto& output_frame = packet.Get<mediapipe::ImageFrame>();

    // Get the packet containing multi_hand_landmarks.
     mediapipe::Packet multi_hand_landmarks_packet;
    if (!multi_hand_landmarks_poller.Next(&multi_hand_landmarks_packet)) break;
    const auto& multi_hand_landmarks =
        multi_hand_landmarks_packet.Get<
            std::vector<mediapipe::NormalizedLandmarkList>>();

    std::cout << "#Multi Hand landmarks: " << multi_hand_landmarks.size() << std::endl;

    int hand_id = 0;
    for (const auto& single_hand_landmarks: multi_hand_landmarks) {
      ++hand_id;
      std::cout << "Hand [" << hand_id << "]:";
      for (int i = 0; i < single_hand_landmarks.landmark_size(); ++i) {
        const auto& landmark = single_hand_landmarks.landmark(i);
	std::cout << "\tLandmark [" << i << "]: ("
                  << landmark.x() << ", "
                  << landmark.y() << ", "
                  << landmark.z() << ")";
      }
    }

    // Convert back to opencv for display or saving.
    cv::Mat output_frame_mat = mediapipe::formats::MatView(&output_frame);
    cv::cvtColor(output_frame_mat, output_frame_mat, cv::COLOR_RGB2BGR);

    cv::imshow(kWindowName, output_frame_mat);
    // Press any key to exit.
    const int pressed_key = cv::waitKey(5);
    if (pressed_key >= 0 && pressed_key != 255) grab_frames = false;
  }
}

int main(int argc, char** argv)
{
  RunMPPGraph();
  return EXIT_SUCCESS;
}
