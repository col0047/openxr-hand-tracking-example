g++ src/main.cpp \
-I deps/mediapipe \
-I deps/absl \
-I deps/bin/ \
-I deps/protobuf/ \
-I deps/github_glog/ \
-I deps/gflags/ \
-I deps/google_glog/  \
`pkg-config opencv --cflags --libs` -pthread  \
libmediapipe_external.so libmediapipe_internal.so  \
-fpermissive -std=c++17 \
-o monado-hand-tracking-debug
