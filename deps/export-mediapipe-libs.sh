#!/usr/bin/env bash

CC=$(which gcc)
NM=$(which nm)

# Check if MEDIAPIPE_HOME is defined.
if [[ -z "${MEDIAPIPE_HOME}" ]]; then
  >&2 echo "MEDIAPIPE_HOME not set."
  exit 1
fi

# For the external objects.
for ext_obj in $(find ${MEDIAPIPE_HOME}/bazel-bin/external/ -name '*.o' ); do
  ext_obj_arr+=($ext_obj)
done
$($CC ${ext_obj_arr[@]} -shared -o libmediapipe_external.so)
if [ $? -ne 0 ]; then
  exit 1
fi
$(mv libmediapipe_external.so $1)

# For the internal objects.
for int_obj in $(find ${MEDIAPIPE_HOME}/bazel-bin/mediapipe/ -name '*.o' ); do
  if [[ $($NM -Ca $int_obj) != *"T main"* ]]; then
    int_obj_arr+=($int_obj)
  fi
done
out=$($CC ${int_obj_arr[@]} -shared -o libmediapipe_internal.so)
if [ $? -ne 0 ]; then
  exit 1
fi
$(mv libmediapipe_internal.so $1)

exit 0
